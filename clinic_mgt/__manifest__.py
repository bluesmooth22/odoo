# -*- coding: utf-8 -*-
{
    'name': "Clinic Management",

    'summary': """
        Clinic Management Systems""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Daryll Gay Bangoy",
    'website': "http://www.linkedin.com/lyradb",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','base_setup','mail','contacts', 'portal', 'sale_management', 'board', #'jasper_reports',
                'account', 'field_image_preview', 'auth_session_timeout', 'smile_web_auto_refresh'],

    # always loaded
    'data': [
        'security/security.xml',
        'data/data.xml',
        # 'views/base_view.xml',
        'security/ir.model.access.csv',
        'views/billing_views.xml',
        'views/cashier_recon_view.xml',
        'views/views.xml',
        'views/config_md_pf.xml',
        'views/sale_order.xml',
        'views/account_invoice.xml',
        'views/cash_recon_view_v2.xml',
        'reports/templates/sales_summary_template_v2.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}