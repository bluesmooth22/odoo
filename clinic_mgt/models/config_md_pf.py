from odoo import models, fields, api, _, SUPERUSER_ID, tools
from odoo.exceptions import ValidationError
from odoo.modules.module import get_module_resource
from datetime import timedelta, date, datetime
from dateutil.relativedelta import relativedelta
import base64, platform, rsa
import odoo.addons.decimal_precision as dp


class configMdPf(models.Model):
    _inherit = 'config.md.pf'


class clinicFindings(models.Model):
    _inherit = 'clinic.findings'

    config_md_pf_id = fields.Many2one(comodel_name='config.md.pf', string='Professional Fee')
    pf_vat = fields.Monetary(string='VAT', currency_field='pf_currency_id',related='config_md_pf_id.pf_vat')
    pf_discount = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'), default=0.0
                                  ,related='config_md_pf_id.pf_discount')
    pf_net = fields.Monetary(string='Net', currency_field='pf_currency_id', related='config_md_pf_id.pf_net')

    hospital_id = fields.Many2one(
        string="Hospital Name",
        comodel_name="res.partner",
        domain="[('is_company', '=', True),('is_hospital','=',True)]",
        context={"default_company_type": "company", "default_is_company": True, "default_is_hospital": True},
        track_visibility=True
    )


class clinicAdmission(models.Model):
    _inherit = 'clinic.admission'

    config_md_pf_id = fields.Many2one(comodel_name='config.md.pf', string='Professional Fee')
    pf_vat = fields.Monetary(string='VAT', currency_field='pf_currency_id', related='config_md_pf_id.pf_vat')
    pf_discount = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'), default=0.0
                               , related='config_md_pf_id.pf_discount')
    pf_net = fields.Monetary(string='Net', currency_field='pf_currency_id', related='config_md_pf_id.pf_net')


class clinicBooking(models.Model):
    _inherit = 'clinic.booking'

    tax_id = fields.Many2one(comodel_name='account.tax', string='Tax', related='config_md_pf_id.tax_id')

    @api.onchange("config_md_pf_id")
    def _onchange_config_md_pf_id(self):
        for rec in self:
            rec.pf = rec.config_md_pf_id.pf
            rec.pf_currency_id = rec.config_md_pf_id.pf_currency_id

    @api.multi
    def action_check_order(self):
        taxes = []
        if not self.order_id:

            if not self.config_md_pf_id:
                res = self.env['config.md.pf'].search([('effectivity_date','<=',self.booking_date),
                                                                        ('user_id','=',self.physician_id.id)],order='effectivity_date desc',limit=1)

                if not res:res = self.env['config.md.pf'].search([('effectivity_date','>=',self.booking_date),
                                                                        ('user_id','=',self.physician_id.id)],order='effectivity_date asc',limit=1)

                if not res:
                    raise ValidationError(_('Please set Physician Consultation Fee first of %s.') % (self.physician_id.name))
                config_md_pf_id = res
                pf = config_md_pf_id.pf
                pf_currency_id = config_md_pf_id.pf_currency_id
            else:
                config_md_pf_id = self.config_md_pf_id
                pf = config_md_pf_id.pf
                pf_currency_id = config_md_pf_id.pf_currency_id
            tax_id = self.tax_id.id or  self.config_md_pf_id.tax_id.id
            taxes.append(tax_id)
            inv = self.env.ref('clinic_mgt.product_clinic_pf')
            pricelist_id = self.env.ref('product.list0').id or False
            partner_id = self.patient_id.id
            uom = self.env.ref('uom.product_uom_unit').id
            self.env['product.product'].browse(inv)

            context =  {
                    'default_state': 'draft',
                    'default_reference': self.name,
                    'default_partner_id': partner_id,
                    'default_partner_invoice_id': partner_id,
                    'default_partner_shipping_id': partner_id,
                    'default_confirmation_date': fields.datetime.now().date(),
                    'default_pricelist_id': pricelist_id,
                    'default_is_pwd': config_md_pf_id.pf_with_discount,
                    'default_order_line': [(0,0,{'name':'Consultation', 'display_type': 'line_section'}),
                        (0,0,{
                            'product_id': inv.id,
                            'name': inv.name,
                            'product_uom': uom,
                            'product_uom_qty': 1.00,
                            'qty_delivered': 1.00,
                            'price_unit': pf,
                            'discount': config_md_pf_id.pf_discount,
                            'price_discount': config_md_pf_id.price_discount,
                            'tax_id': [(4, config_md_pf_id.tax_id.id)]
                        }),
                        (0,0,{'name':'Medicines', 'display_type': 'line_section'})],
                    'default_clinic_booking_id':self.id,
                    'default_currency_id': pf_currency_id.id,
                    'default_company_currency_id': pf_currency_id.id,
                    }
            domain = [('state','=','sale')]
            view_id = self.env.ref('sale.view_order_form').id

            target = 'new'

            ret = {'type': 'ir.actions.act_window',
                    'name': _('Sales Order(s)'),
                    'view_type': 'form',
                    'view_mode': 'form',
                    'view_id': view_id,
                    'res_model': 'sale.order',
                    'target': target,
                    'domain': domain,
                    'context': context,}
        else:

            ret = {
                'name': _('Sales Order(s)'),
                'type': 'ir.actions.act_window',
                'res_model': 'sale.order',
                'target': 'current',
                'res_id': self.order_id.id,
                'view_mode': 'form',
                }

        return ret


class saleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    price_discount = fields.Float('Whole Price Discount', required=True, digits=dp.get_precision('Product Price'), default=0.0)

    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id', 'price_discount')
    def _compute_amount(self):
        for line in self:
            price = line.price_unit
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
                                            product=line.product_id, partner=line.order_id.partner_shipping_id)
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'] * (1 - (line.discount or 0.0) / 100.0) - line.price_discount,
            })

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(saleOrderLine, self)._prepare_invoice_line(qty)
        res['price_discount'] = self.price_discount or 0.0
        res['price_tax'] = self.price_tax or 0.0
        if self.price_discount:
            res['price_discount'] = self.price_discount

        if self.price_tax:
            res['price_tax_'] = self.price_tax
        return res


class saleOrder(models.Model):
    _inherit = 'sale.order'


    is_pwd = fields.Boolean(string='Is SC/PWD')

    @api.depends('order_line.price_total', 'order_line.tax_id', 'is_pwd')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            if order.is_pwd:
                amount_untaxed = amount_tax = 0.0
                for line in order.order_line:
                    amount_untaxed += line.price_subtotal
                    amount_tax += line.price_tax
                amount_untaxed = amount_untaxed - amount_tax
                order.update({
                    'amount_untaxed': amount_untaxed,
                    'amount_tax': amount_tax,
                    'amount_total': amount_untaxed + amount_tax,
                })
            else:
                amount_untaxed = amount_tax = 0.0
                for line in order.order_line:
                    amount_untaxed += line.price_subtotal
                    amount_tax += line.price_tax
                order.update({
                    'amount_untaxed': amount_untaxed,
                    'amount_tax': amount_tax,
                    'amount_total': amount_untaxed + amount_tax,
                })

    @api.multi
    def _prepare_invoice(self):
        invoice_vals = super(saleOrder, self)._prepare_invoice()
        invoice_vals['is_pwd'] = self.is_pwd or False
        if self.is_pwd:
            invoice_vals['is_pwd'] = self.is_pwd
        return invoice_vals