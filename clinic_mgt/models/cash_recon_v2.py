from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class cashReconV2Config(models.Model):
    _name = 'cash.recon.v2.config'
    _rec_name = 'product_id'

    @api.multi
    def _default_product(self):
        res_id = self.env.ref('clinic_mgt.product_clinic_pf').id
        product = self.env['product.product'].browse(res_id)
        return product.id

    product_id = fields.Many2one(comodel_name='product.product', string='Product', default=_default_product)

    @api.model
    def create(self, vals):
        cnt = self.env['cash.recon.v2.config'].search_count([])
        print(cnt)
        if cnt >= 1:
            raise ValidationError(
                _("You can only create one!"))
        return super(cashReconV2Config, self).create(vals)


class cashReconV2(models.Model):
    _name = 'cash.recon.v2'
    _rec_name = 'recon_date'

    recon_date = fields.Date(string='Cash Reconciliation Date', default=fields.Date.today())
    pf_currency_id = fields.Many2one('res.currency', string='Currency',
                                     default=lambda self: self.env.user.company_id.currency_id.id)
    profession_fee = fields.Monetary(string='Professional Fee', currency_field='pf_currency_id', compute='_get_amount')
    med_fee = fields.Monetary(string='Medicines', currency_field='pf_currency_id', compute='_get_amount')
    other_fees = fields.Monetary(string='Other Fees', currency_field='pf_currency_id', compute='_get_amount')
    discount_fee = fields.Monetary(string='Discount', currency_field='pf_currency_id', compute='_get_amount')
    total_amount = fields.Monetary(string='Total Amount', store=True, currency_field='pf_currency_id',
                                   compute='_get_amount')
    paid_amount = fields.Monetary(string='Amount Paid', store=True, currency_field='pf_currency_id',
                                  compute='_get_amount')
    residual = fields.Monetary(string='Balance', store=True, currency_field='pf_currency_id', compute='_get_amount')
    cash_recon_line_v2_ids = fields.One2many(comodel_name='cash.recon.line.v2', inverse_name='cash_recon_v2_id',
                                             string='Reconciliation Line', required=False)
    cc_ids = fields.One2many(string="Cash Count", comodel_name="cashier.cashcount.denom", inverse_name="cash_recon_v2_id", )
    total_cash_count = fields.Monetary(string='Total Cash Count', store=True, currency_field='pf_currency_id', compute='_get_cash_count')
    total_cash_count_vs_paid_amount = fields.Monetary(string='Short [-]/Over [+]', store=True, currency_field='pf_currency_id',
                                  compute='_get_cash_count')

    @api.model
    def create(self, vals):
        res = super(cashReconV2, self).create(vals)
        ids = []
        if res:
            den = self.env['config.denomination'].search([("active","=",True)])
            for rec in den:
                ids.append((0, 0, {'cash_recon_v2_id': res, 'denomination': rec.denomination}))
            res.write({
                'cc_ids': ids
            })
        return res

    state = fields.Selection(string='Status', selection=[
        ('draft', 'Draft'),
        ('done', 'Done'),
        ('cancel', 'Cancel')
    ], default='draft')

    @api.depends('cash_recon_line_v2_ids')
    def _get_amount(self):
        for rec in self:
            profession_fee = 0; med_fee = 0; other_fees = 0; discount_fee = 0; paid_amount = 0
            for record in rec.cash_recon_line_v2_ids:
                profession_fee += record.profession_fee
                med_fee += record.med_fee
                other_fees += record.other_fees
                discount_fee += record.discount_fee
                paid_amount += record.paid_amount

            total_amount = profession_fee + med_fee + other_fees - discount_fee
            residual = total_amount - paid_amount
            rec.update({
                'profession_fee': profession_fee,
                'med_fee': med_fee,
                'other_fees': other_fees,
                'discount_fee': discount_fee,
                'total_amount': total_amount,
                'paid_amount': paid_amount,
                'residual': residual
            })

    @api.depends('cc_ids')
    def _get_cash_count(self):
        for record in self:
            total_cash_count = 0;
            for rec in record.cc_ids:
                total_cash_count += rec.total

            record.update({
                'total_cash_count': total_cash_count,
                'total_cash_count_vs_paid_amount': total_cash_count - record.paid_amount
            })


    @api.multi
    def get_invoices(self):
        currency = self.pf_currency_id or None
        recon_config = self.env['cash.recon.v2.config'].search([], limit=1)
        recon_line = self.env['cash.recon.line.v2']
        product = self.env['product.product']
        sales = self.env['sale.order']
        booking = self.env['clinic.booking']

        pro_fee = product.search([('id', '=', recon_config.product_id.id)])
        med = product.search([('id', '!=', recon_config.product_id.id), ('type', '!=', 'service')])
        other = product.search([('id', '!=', recon_config.product_id.id), ('type', '=', 'service')])

        if recon_line.search([('cash_recon_v2_id', '=', self.id)]):
            recon_line.search([('cash_recon_v2_id', '=', self.id)]).unlink()

        invLineProFee  = 0; invLineProTax = False; invLineProSubtotal = 0; invLineProDiscount = 0
        invLineMedFee  = 0; invLineMedTax = False; invLineMedSubtotal = 0; invLineMedDiscount = 0
        invLineMedTaxExcludedTotal = 0; invLineMedDiscountTotal = 0

        invLineOtherFee = 0; invLineOtherTax = False; invLineOtherSubtotal = 0; invLineOtherDiscount = 0
        invLineOtherTaxExcludedTotal = 0; invLineOtherDiscountTotal = 0
        total_amount = 0; total_amount_paid = 0

        # print(self.recon_date.strftime("%m/%d/%Y"))
        invoice = self.env['account.invoice'].search([('state', 'not in', ('draft', 'cancel')), ('date_invoice', '=', self.recon_date.strftime("%m/%d/%Y"))])
        invoice_line = self.env['account.invoice.line']
        for inv in invoice:
            sales = sales.search([('partner_id', '=', inv.partner_id.id), ('name', '=', inv.origin)])
            booking = booking.search([('id', '=', sales.clinic_booking_id.id)])
            # invoice line professional fee
            invLinePro = invoice_line.search([('invoice_id', '=', inv.id), ('product_id', '=', pro_fee.id)], limit=1)
            invLineProFee += invLinePro.price_unit
            invLineProTax = invLinePro.invoice_line_tax_ids.compute_all(invLineProFee, currency, invLinePro.quantity,
                                                                        product=invLinePro.product_id,
                                                                        partner=invLinePro.invoice_id.partner_id)
            invLineProSubtotal = (invLineProTax['total_excluded'] if invLineProTax else invLinePro.quantity * invLineProFee) * \
            (1 - (invLinePro.discount or 0.0) / 100) - invLinePro.price_discount
            invLineProDiscount = invLineProFee - invLineProSubtotal

            # invoice line Meds
            invLineMed = invoice_line.search([('invoice_id', '=', inv.id), ('product_id', 'in', med.ids)])
            for invLine in invLineMed:

                invLineMedFee += invLine.price_unit
                invLineMedTax = invLine.invoice_line_tax_ids.compute_all(invLine.price_unit, currency, invLine.quantity,
                                                                         product=invLine.product_id,
                                                                         partner=invLine.invoice_id.partner_id)
                invLineMedTaxExcluded = invLineMedTax['total_excluded'] if invLineMedTax else invLine.quantity * invLine.price_unit
                invLineMedTaxExcludedTotal += invLineMedTaxExcluded
                invLineMedDiscount = invLineMedTaxExcluded * (invLine.discount or 0.0 / 100)
                invLineMedDiscount += invLine.price_discount
                invLineMedDiscountTotal += invLineMedDiscount


            invLineMedSubtotal = invLineMedTaxExcludedTotal - invLineMedDiscountTotal

            # invoice line other fees
            invLineOther = invoice_line.search([('invoice_id', '=', inv.id), ('product_id', 'in', other.ids)])
            for invLine in invLineOther:

                invLineOtherFee += invLine.price_unit
                invLineOtherTax = invLine.invoice_line_tax_ids.compute_all(invLine.price_unit, currency, invLine.quantity,
                                                                         product=invLine.product_id,
                                                                         partner=invLine.invoice_id.partner_id)
                invLineOtherTaxExcluded = invLineOtherTax['total_excluded'] if invLineOtherTax else invLine.quantity * invLine.price_unit
                invLineOtherTaxExcludedTotal += invLineOtherTaxExcluded
                invLineOtherDiscount = invLineOtherTaxExcluded * (invLine.discount or 0.0 / 100)
                invLineOtherDiscount += invLine.price_discount
                invLineOtherDiscountTotal += invLineOtherDiscount

            # print(invLineOtherTaxExcluded, invLineOtherDiscount)
            invLineOtherSubtotal = invLineOtherTaxExcludedTotal - invLineOtherDiscountTotal
            invLineDiscountTotal = invLineOtherDiscountTotal + invLineMedDiscountTotal + invLineProDiscount
            total_amount = inv.amount_total
            total_amount_paid = total_amount - inv.residual

            recon_line.search([]).create(
                {
                    'cash_recon_v2_id': self.id,
                    'invoice_id': inv.id,
                    'pf_currency_id': currency.id,
                    'patient_id': inv.partner_id.id,
                    'profession_fee': invLineProFee,
                    'med_fee': invLineMedSubtotal,
                    'other_fees': invLineOtherSubtotal,
                    'discount_fee': invLineDiscountTotal,
                    'paid_amount': total_amount_paid,
                    'booking_id': booking.id,
                }
            )

    def action_state(self):
        self.ensure_one()
        status = self._context['status']
        if status == 'draft':
            self.state = 'draft'
        elif status == 'done':
            self.state = 'done'
        else:
            self.state = 'cancel'


    _sql_constraints = [
        ('recon_date_unique', 'unique(recon_date)', 'You have created your daily reconciliation, Please check and update!')
    ]

class cashReconLineV2(models.Model):
    _name = 'cash.recon.line.v2'

    cash_recon_v2_id = fields.Many2one(comodel_name='cash.recon.v2', string='Cash Reconciliation')
    invoice_id = fields.Many2one(comodel_name='account.invoice', string='Invoices')
    pf_currency_id = fields.Many2one('res.currency', string='Currency',
                                     default=lambda self: self.env.user.company_id.currency_id.id)
    booking_id = fields.Many2one(comodel_name='clinic.booking', string='Booking')
    sequence = fields.Integer(string="Priority #", related='booking_id.sequence')
    booking_date = fields.Date(string='Consultation Date', related='booking_id.booking_date')
    patient_id = fields.Many2one(comodel_name='res.partner', string='Patient')

    profession_fee = fields.Monetary(string='Professional Fee', store=True,
                                     currency_field='pf_currency_id')
    med_fee = fields.Monetary(string='Medicines', store=True,
                              currency_field='pf_currency_id')
    other_fees = fields.Monetary(string='Other Fees', store=True,
                                currency_field='pf_currency_id')
    discount_fee = fields.Monetary(string='Discount', store=True,
                                   currency_field='pf_currency_id')
    paid_amount = fields.Monetary(string='Amount Paid', store=True,
                                  currency_field='pf_currency_id')

class CashierCashcountDenom(models.Model):
    _inherit = 'cashier.cashcount.denom'

    cash_recon_v2_id = fields.Many2one(comodel_name="cash.recon.v2", string="Cash Count", required=False)


