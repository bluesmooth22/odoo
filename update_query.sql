update clinic_booking set bp=sugar where not sugar isnull and bp isnull;

-- res_partner
select concat('partner_id_',replace(cAddressedTo,' ','')) as id, 
cAddressedTo as name, cAddress1 as street, cAddress2 as street2, 'True' is_physician from tblreferral group by 1 order by 1,2;

-- admission
select
concat('admission_id_',cInPatientCode) as id,
cPatientName as patient_id,
replace(dAdmittedDate,'8201','2011') as admitted_date,
dDischargedDate as discharge_date,
COALESCE(datediff(dDischargedDate,replace(dAdmittedDate,'8201','2011')),0) as days_admitted,
(case when nProfessionalFee>0.0 then nProfessionalFee else nAmountPaid end) as pf,
nPHICAmount as PHIC,
nProfessionalFee-nPHICAmount as amount_due,
nAmountPaid as amount_paid,
(case when nProfessionalFee>0.0 then nProfessionalFee else nAmountPaid end)-nPHICAmount-nAmountPaid as balance,
'NATIVIDAD M. TORRE, MD.' as physician_id,
(case  when lvoid then 'void' 
	when ((case when nProfessionalFee>0.0 then nProfessionalFee else nAmountPaid end)-nPHICAmount-nAmountPaid)!=0.00 then 'partial' 
	else 'paid' end) as state
from tblinpatient a
INNER JOIN tblpatient c on a.nPatientID = c.nPatientID;